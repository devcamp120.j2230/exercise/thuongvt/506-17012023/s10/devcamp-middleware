const express = require("express");

const app = express();

const prot = 8000;

//Middeware 
app.use((req,res,next)=>{
    console.log(new Date());

    next();
})

app.get("/",(req,res)=>{
    let day = new Date();
    console.log(`xin chào hôm nay là ngày ${day.getDate()} tháng ${day.getMonth()+1} năm ${day.getFullYear()}`)
    res.json({
        message: `xin chào hôm nay là ngày ${day.getDate()} tháng ${day.getMonth()+1} năm ${day.getFullYear()}`
    })
})

app.post("/",(req,res)=>{
    console.log("Post Method");
    res.status(200).json({
        message: "post method"
    })
})
// // Khai báo API dạng get
// app.get("/get-method",(req,res)=>{
//     res.json({
//         message:"get method"
//     })
// })

// // Khai báo API dạng POST
// app.post("/post-method",(req,res)=>{
//     res.json({
//         message:"post method"
//     })
// })

// // Khai báo API dạng PUT
// app.put("/put-method",(req,res)=>{
//     res.json({
//         message:"put method"
//     })
// })

// //Khai báo API dạng DELETE
// app.delete("/delete-method", (req,res)=>{
//     res.json({
//         message:"delete method"
//     })
// })

app.listen(prot, ()=>{
    console.log("app listen on prot", prot)
});
